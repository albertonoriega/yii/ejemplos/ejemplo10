<?php

echo \yii\widgets\DetailView::widget([
    "model"=> $model,
    'attributes' => [
        [
            'attribute' => 'Imagen',
            'format' => 'raw',
            'value' => function ($model){
                return \yii\helpers\Html::img("@web/imgs/" . $model->nombre, ["class" => "img-thumbnail"]);
                // $model->nombre es lo mismo que poner $model->nombre->name. Esto sucede porque al poner $model->nombre llama al método toString() y te pone el nombre directamente
            }
        ]
    ],
]);

