<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario2 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio2">

    <?php $form = ActiveForm::begin(); 

        echo $form->field($model, 'nombre') // campo nombre
                ->textInput(
                        ['placeholder' => 'Escriba su nombre',]
                );  // mostrar como caja de texto
        
        echo $form->field($model, 'poblacion') // campo poblacion
                ->dropDownList($model->poblaciones(),
                        ['prompt' => 'Seleccione una poblacion',] // conseguimos que en la lista te salga por defecto un mensaje 
                        ); // lista desplegable (solo puedes seleccionar una de las opciones de la lista
        
        echo $form->field($model, 'color')
                ->listBox($model->colores()); // cuadro de lista
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio2 -->
