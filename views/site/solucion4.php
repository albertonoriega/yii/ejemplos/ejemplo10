<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "diaTexto", // como diaTexto y mesTexto son propiedades private en el modelo, llamará a sus getter
        "mesTexto",
        
        // Si no tenemos en el modelo los getter para convertir los array a string lo hacemos de la siguiente forma
//        [
//            "attribute" => "Mes como texto",
//            "value" => function ($model){
//                return join(",", $model->mes);
//            }
//        ],
//        [
//            "label" => "Dia como texto",
//            "value" => function ($model){
//                return join(",", $model->dia);
//            }
//        ]        
    ],
]);
