<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario5 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio5">

    <?php $form = ActiveForm::begin(); 

        echo $form->field($model, 'nombre')->textInput([
            "placeholder" => "Introducir nombre",
        ]); 
        echo $form->field($model, 'apellidos')->textInput([
            "placeholder" => "Introducir apellidos",
        ]); 
        echo $form->field($model, 'fechaNacimiento')->input("date"); 
        echo $form->field($model, 'correo')->input("email");
        echo $form->field($model, 'poblacion')
                ->dropDownList($model->poblaciones(),
                    ['prompt' => 'Seleccione una población'],
                );
        echo $form->field($model, 'mesesAcceso')
                ->checkboxList($model->meses()); 
    ?>
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio5 -->
