<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = 'Ejemplo 10';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 10 - Trabajo con Formularios</h1>

        <p class="lead">Trabajo con formularios activos y controles avanzados</p>
        <p class="lead">Trabajo con controles de extensiones externas (cargar componentes)</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Formulario 1</h2>
                <div>
                   
                    Hacer un formulario con nombre, direccion, edad y fecha
                     <ul>
                    <li>Creamos el modelo Formulario 1 con sus propiedades, attributeLabels y rules </li> 
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio1:
                        <ul>
                            <li>Nombre => textInput()</li>
                            <li>Direccion => Text area</li>
                            <li>Edad => input("number")</li>
                            <li>Fecha => input("date")</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion1</li>
                    <li>Mostramos los datos con un DetailView</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 1", // label
                        ["site/ejercicio1"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 2</h2>
                <div>
                   
                    Hacer un formulario con nombre, población en una lista desplegable y el color en una lista
                     <ul>
                    <li>Creamos el modelo Formulario 2 con sus propiedades, attributeLabels y rules </li>
                    <li>Ademas en el modelo creamos dos metodos con un array en cada metodo con las poblaciones y los colores </li> 
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio2:
                        <ul>
                            <li>Nombre => textInput()</li>
                            <li>Poblacion => dropDownList</li>
                            <li>Color => listBox</li>
                    </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion1</li>
                    <li>Mostramos los datos con un DetailView</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 2", 
                        ["site/ejercicio2"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 3</h2>
                <div>
                   
                    Hacer un formulario que te permite seleccionar un mes y un dia
                     <ul>
                    <li>Creamos el modelo Formulario 3 con sus propiedades, attributeLabels y rules </li>
                    <li>Ademas en el modelo creamos dos metodos con un array en cada metodo con los meses y los días </li> 
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio3:
                        <ul>
                            <li>Meses en un RadioList con 12 radioButtons</li>
                            <li>Días en un RadioList con 5 radioButtons</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion1</li>
                    <li>Mostramos los datos con un DetailView</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 3", 
                        ["site/ejercicio3"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 4</h2>
                <div>
                   
                    Hacer un formulario que te permite seleccionar mas de un mes y mas de un dia
                     <ul>
                    <li>Creamos el modelo Formulario 4 con sus propiedades, attributeLabels y rules </li>
                    <li>Para seleccionar mas de un elemento ponemos las propiedades como array </li> 
                    <li>Creamos dos propiedades privadas y dos getter de dichas propiedades para convertir los array en string</li> 
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio4:
                        <ul>
                            <li>Meses con un CheckBoxList</li>
                            <li>Días con un ListBox ["multiple"]</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion4</li>
                    <li>Mostramos los datos con un DetailView, tenemos que poner un array de attributes para mostrarlo</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 4", 
                        ["site/ejercicio4"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 5</h2>
                <div>
                   
                    Hacer un formulario que contenga Nombre, Apellidos, Fecha, Correo, Lista desplegable de poblaciones y CheckBox de meses de verano
                     <ul>
                    <li>Creamos el modelo Formulario 5 con sus propiedades, attributeLabels y rules </li>
                    <li>Creamos las propiedades privadas y los metodos para generar la lista desplegable y los checkBox </li> 
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio5:
                        <ul>
                            <li>Nombre como un textInput</li>
                            <li>Apellidos como un textInput</li>
                            <li>Fecha => input("date") </li>
                            <li>Correo => input("email")</li>
                            <li>Poblacion => dropDownList</li>
                            <li>Meses con un CheckBoxList</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion5</li>
                    <li>Mostramos los datos con un DetailView, tenemos que poner un array de attributes para mostrarlo</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 5", 
                        ["site/ejercicio5"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 6</h2>
                <div>
                   
                    Hacer un formulario que te permita subir un archivo
                     <ul>
                    <li>Creamos el modelo Formulario 6 con su propiedad, attributeLabel y rules </li>
                    <li>Heredamos los metodos beforeValidate y afterValidate en el modelo para poder subir archivos</li>
                    <li>Creamos un metodo en el modelo llamado subirArchivo que nos permite guardar los archivos subidos en la carpeta web </li>
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio5:
                        <ul>
                            <li>Imagen como FileInput</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion6</li>
                    <li>Mostramos los datos con un DetailView, tenemos que poner un array de attributes para mostrarlo y usar el Hmtl:: img para mostrar la foto</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 6", 
                        ["site/ejercicio6"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Formulario 7</h2>
                <div>
                   
                    Hacer un formulario que contenga dos textInput, fecha, un checkbox, un ListBox. Se podrá también subir archivos
                     <ul>
                    <li>Creamos el modelo Formulario 7 con su propiedades, attributeLabels y rules </li>
                    <li>Creamos las propiedades privadas y los metodos para generar el listBox y los checkBox </li>
                    <li>Heredamos los metodos beforeValidate y afterValidate en el modelo para poder subir archivos</li>
                    <li>Creamos un metodo en el modelo llamado subirArchivo que nos permite guardar los archivos subidos en la carpeta web </li>
                    <li>Creamos el Formulario con gii </li>
                    <li>En la vista ejercicio7:
                        <ul>
                            <li>Nombre como textInput</li>
                            <li>Identificador como textInput</li>
                            <li>Fecha => input("date)</li>
                            <li>Opciones como CheckBox</li>
                            <li>Elementos como ListBox</li>
                            <li>Imagen como FileInput</li>
                        </ul>
                    </li>
                    <li>En el controlador mandamos a el modelo a la vista solucion7</li>
                    <li>Mostramos los datos con un DetailView, tenemos que poner un array de attributes para mostrarlo y usar el Hmtl:: img para mostrar la foto</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 7", 
                        ["site/ejercicio7"],  
                        ["class"=> "btn btn-primary"]) 
                ?></p>
            </div>
            

    </div>
</div>
