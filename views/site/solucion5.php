<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "nombre",
        "apellidos",
        "fechaNacimiento",
        "correo",
        "poblacion",
        "mesesTexto",
    ],
]);
