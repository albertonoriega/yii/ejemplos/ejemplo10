<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario4 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio4">

    <?php $form = ActiveForm::begin(); 

        echo $form->field($model, 'mes') // campo
                ->checkboxList($model->meses()); // casillas (se pueden seleccionar mas de una a la vez)
        echo $form->field($model, 'dia') // campo
                ->listBox($model->dias(),[  // lista de elementos
                    "multiple" => "multiple", // para que podamos escoger mas de uno de los items de la lista
                ]); 
    
    ?>
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio4 -->
