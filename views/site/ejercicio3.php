<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario3 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio3">

    <?php $form = ActiveForm::begin(); 

        echo $form->field($model, 'mes') // campo mes
                ->radioList($model->meses()); // botones de radio (solo puedes seleccionar uno)
        
        echo $form->field($model, 'dia') // campo dia
                ->radioList($model->dias()) 
        
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio3 -->
