<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario1 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio1">

    <?php $form = ActiveForm::begin(); ?>
<!--         con text input podemos ponerle un array con las opciones -->
        <?php
            echo $form->field($model, 'nombre') // campo a utilizar
                    ->textInput([
            "placeholder" => "Introduce nombre completo"
        ]); // colocar caja de texto
            
            echo  $form->field($model, 'direccion') // campo direccion
                ->textarea([
                    "cols" => 30,
                    "rows" => 5,
                ]);  // colocar area de texto mediante su metodo textarea
            
            echo $form->field($model, 'edad') // campo edad
                    ->input('number'); // input type=number. No existe en Yii en metodo que genere inpputs number
            
            echo $form->field($model, 'fecha') // campo fecha
                    ->input('date');  // input type=date 
                    
        ?>
    
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio1 -->
