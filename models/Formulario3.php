<?php



namespace app\models;


class Formulario3 extends \yii\base\Model {
    public string $mes="";
    public string $dia="";
    
    public function attributeLabels(): array {
        return [
            'mes' => 'Mes comienzo',
            'dia' => 'Día comienzo'
        ]; 
        
    }
    public function rules(): array {
        return [
            [['mes','dia'],'safe'] // safe no obliga a nada pero te permite cargar los datos del formulario. Sin esto no los lee
        ];
    }
    
    public function dias():array {
        return ['Lunes','Martes','Miercoles','Jueves','Viernes'];
    }
    
    public function meses(): array {
        return [
            'Enero'=> 'Enero',
            'Febrero'=> 'Febrero',
            'Marzo'=> 'Marzo',
            'Abril'=> 'Abril',
            'Mayo'=> 'Mayo',
            'Junio'=> 'Junio',
            'Julio'=> 'Julio',
            'Agosto'=> 'Agosto',
            'Septiembre'=> 'Septiembre',
            'Octubre'=> 'Octubre',
            'Noviembre'=> 'Noviembre',
            'Diciembre'=> 'Diciembre',
            
        ];
    }
}
