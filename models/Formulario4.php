<?php



namespace app\models;


class Formulario4 extends \yii\base\Model {
    public array $mes=[]; // Para que podamos escoger mas de un mes, tipamos como array
    public array $dia=[];
    private string $mesTexto=""; // ponemos estas dos propiedades para que llame a sus getteer
    private string $diaTexto="";


    public function attributeLabels(): array {
        return [
            'mes' => 'Mes comienzo',
            'dia' => 'Día comienzo'
        ]; 
        
    }
    public function rules(): array {
        return [
            [['mes','dia'],'safe'] // safe no obliga a nada pero te permite cargar los datos del formulario. Sin esto no los lee
        ];
    }
    
    public function dias():array {
        return ['Lunes','Martes','Miercoles','Jueves','Viernes'];
    }
    
    public function meses(): array {
        return [
            'Enero'=> 'Enero',
            'Febrero'=> 'Febrero',
            'Marzo'=> 'Marzo',
            'Abril'=> 'Abril',
            'Mayo'=> 'Mayo',
            'Junio'=> 'Junio',
            'Julio'=> 'Julio',
            'Agosto'=> 'Agosto',
            'Septiembre'=> 'Septiembre',
            'Octubre'=> 'Octubre',
            'Noviembre'=> 'Noviembre',
            'Diciembre'=> 'Diciembre',
            
        ];
    }
    
    public function getMesTexto(): string {
        // La funcion join te convierte un array en un string con el separador que tu quieras (en este caso una coma)
        return join(",", $this->mes);
    }

    public function getDiaTexto(): string {
        return join(",", $this->dia);
    }


}
