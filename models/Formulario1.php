<?php

namespace app\models;

use yii\base\Model;

class Formulario1 extends Model{
    public ?string $nombre=null; // caja de texto
    public ?string $direccion=null; // area de texto
    public ?int $edad=null; // input tipo numero
    public ?string $fecha=null; //input de tipo date
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Nombre completo',
            'direccion' => 'Dirección',
            'edad' => 'Edad',
            'fecha' => 'Fecha Entrada',
        ];
    }
    
    public function rules(): array {
        return [
            [['nombre','direccion','edad','fecha'],'required']
        ];
    }
}
