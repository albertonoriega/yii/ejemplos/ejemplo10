<?php

namespace app\models;

use yii\base\Model;

class Formulario5 extends Model{
    public string $nombre="";
    public string $apellidos="";
    public string $fechaNacimiento="";
    public string $correo="";
    public string $poblacion="";
    public array $mesesAcceso=[];
    private string $mesesTexto="";
    
    public function attributeLabels(): array {
        return [
            "nombre" => "Nombre",
            "apellidos" => "Apellidos",
            "fechaNacimiento" => "Fecha de nacimiento",
            "correo" => "Correo electrónico",
            "poblacion" => "Población",
            "mesesAcceso" => "Meses de acceso",
        ];
    }
    
    public function rules(): array {
        return [
            [['correo'],'email'], 
            [['nombre','apellidos','fechaNacimiento','poblacion','mesesAcceso'],'safe'], // No metemos correo como safe porque al tener otra regla, ya lo incluye como safe
            
        ];
    }
    
    public function poblaciones () : array {
        return [
            "Santander" => "Santander",
            "Laredo" => "Laredo",
            "Astillero" => "Astillero",
        ];
    }
    
    public function meses(): array {
        return [
            'Junio'=> 'Junio',
            'Julio'=> 'Julio',
            'Agosto'=> 'Agosto',
            'Septiembre'=> 'Septiembre',
        ];
    }
    
    public function getMesesTexto(): string {
        return join(",", $this->mesesAcceso);
    }


}
