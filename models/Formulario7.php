<?php


namespace app\models;


class Formulario7 extends \yii\base\Model{
    
    public string $nombre="";
    public string $identificador="";
    public string $fecha="";
    public array $opciones=[];
    public array $elementos=[];
    private string $listaElementos="";
    private string $listaOpciones="";
    public $imagen;
    
    
    public function attributeLabels(): array {
        return [
            'nombre' => "Nombre",
            'identificador' => "Identificador",
            'fecha' => 'Fecha de entrada',
            'opciones' => 'Opciones',
            'elementos' => 'Elementos disponibles',
            'imagen' => 'Imagen',
        ];
    }
    
    public function rules(): array {
        return [
            [['nombre','identificador','fecha','opciones','elementos'],'required'],
            ['imagen','file','skipOnEmpty' => false,'extensions' => 'jpg,png']
        ];
    }
    
    public function listadoElementos() : array{
        return [
            "opcion 1" => "Opción 1",
            "opcion 2" => "Opción 2",
            "opcion 3" => "Opción 3",
        ];
    }
    
    public function listadoOpciones() : array{
        return [
            "tarjeta de red" => "tarjeta de red",
            "tarjeta de video" => "tarjeta de video",
            "tarjeta de sonido" => "tarjeta de sonido",
            "altavoces" => "altavoces",
        ];
    }
    
    public function getListaOpciones(): string {
        return join(",", $this->opciones);
    }

    public function getListaElementos(): string {
        return join(",", $this->elementos);
    }

    
    public function beforeValidate() {
        $this->imagen = \yii\web\UploadedFile::getInstance($this, "imagen");
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }
    
   
    public function afterValidate() {
        $this->subirArchivo();
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }
    
    public function subirArchivo(): bool {
        $this->imagen->saveAs('imgs/' . $this->imagen->name);
        return true;
    }

}
