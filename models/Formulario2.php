<?php


namespace app\models;

class Formulario2 extends \yii\base\Model{
    public string $nombre="";
    public string $poblacion="";
    public string $color="";
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Nombre del cliente',
            'poblacion' => 'Poblacion del cliente',
            'color' => 'Color elegido',
        ];
    
    }
    public function rules(): array {
        return [
            [['nombre','poblacion','color'],'required']
        ];
    }
    
    // Vamos a crear un metodo que nos permita usar un desplegable de poblaciones
    // La poblaciones que metemos en este metodo, son la que tendremos disponibles para rellenar el campo población
    public function poblaciones () : array {
        return [
            "santander" => "SANTANDER",
            "torrelavega" => "TORRELAVEGA",
            "isla" => "ISLA",
        ];
    }
    
    public function colores () {
        // Creamos un array enumerado para comprobar que da igual que sea enumerado o asociativo
        return [
            'rojo','azul','verde','amarillo',
        ];
    }
}
