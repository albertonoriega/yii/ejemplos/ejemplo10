<?php


namespace app\models;


class Formulario6 extends \yii\base\Model{
    
    // Cuando creamos la propiedad para subir archivos no tipamos la propiedad
    // en teoria tendria que ser un objeto pero al cargar los datos del formulario con el metodo load nos da error
    public $nombre;
    
    public function rules(): array {
        return [
            [['nombre'],'file','skipOnEmpty' => false, 'extensions' => 'jpg,png'],
            // con skiponEmpty => false haces que sea obligatorio subir una foto
            // con extensions delimitamos el tipo de archivo que se puede subir
        ];
    }
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Selecciona imagen',
        ];
    }
    
    
    public function subirArchivo(): bool {
        $this->nombre->saveAs('imgs/' . $this->nombre->name);
        return true;
    }
    
    // Los modelos tienen un metodo llamado beforeValidate() que nos permite escribir codigo para que suceda antes de validar los datos
    // Antes de validar los datos creamos un objeto de tipo UploadFile
    // Si no creamos un objeto de este tipo no podemos trabajar con ficheros porque solo guardaría el nombre del fichero como un string
    public function beforeValidate() {
        $this->nombre = \yii\web\UploadedFile::getInstance($this, "nombre");
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }
    
    // Los modelos tambien tienen otro método que te permite escribit codigo después de validar los datos
    // Cuando se sube una foto, se crea una copia en una carpeta temporal del servidor
    // guardamos la copia de la imagen en la carpeta imgs que creamos en la carpeta web
    public function afterValidate() {
        $this->subirArchivo();
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }
}
